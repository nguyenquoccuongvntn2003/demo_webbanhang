<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'             => 'required',
            'image' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'quantity'         => 'required',
            'price'            => 'required',
            'status'           =>  'required',
            'category_id'      => 'required',
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(response()->json(
            [
                'error' => $errors,
                'status_code' => 422,
            ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
    // public function message()
    // {
    //     return [
    //         'name.required'          => 'không được để trống',
    //         'image.required'         => 'không được để trống',
    //         'quantity.required'      => 'không được để trống',
    //         'price.required'         => 'không được để trống',
    //         'status.required'        => 'không được để trống',
    //         'category_id.required'   => 'không được để trống',
    //     ];
    // }
}
