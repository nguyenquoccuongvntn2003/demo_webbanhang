<?php

namespace App\Http\Resources;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Category;
class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category_id' => new CategoryResource($this->category),
            'image' => $this->image,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'status' => $this->status,
        ];
    }
}
