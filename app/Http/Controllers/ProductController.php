<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\SearchProductRequest;
use App\Services\ProductService;
use Pros\CodeBase\Traits\ResponseTemplateTrait;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use ResponseTemplateTrait;
    protected $productService;
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index($id = null)
    {

        $data = $this->productService->getIndex($id);
        return ProductResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data_result = $request->toArray();
        if ($request->file('image')) {
            $file = $request->file('image');
            $filename = date('YmdHi'). $file->getClientOriginalName();
            $file->move(public_path('images'), $filename);
            $data_result['image'] = $filename;
        }
        $data = $this->productService->getStore($data_result);
        return $this->jsonSuccess($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->productService->getDetail($id);
        return $this->jsonSuccess($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_result = $request->toArray();
        if ($request->file('image')) {
            $file = $request->file('image');
            $filename = date('YmdHi'). $file->getClientOriginalName();
            $file->move(public_path('images'), $filename);
            $data_result['image'] = $filename;
        }
        $data = $this->productService->update($id, $data_result);
        return $this->jsonSuccess($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(SearchProductRequest $request)
    {
        $data = $this->productService->search($request->name);
        return $this->jsonSuccess($data);
    }

    public function destroy($id)
    {
        $data = $this->productService->getDestroy($id);
        return $this->jsonSuccess($data);
    }
}
