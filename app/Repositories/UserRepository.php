<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Collection;
use Pros\CodeBase\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public $user;
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }
    public function getUser($request){
        $user = $this->where('email', $request->email)->first();
        return $user;
    }
}
