<?php

namespace App\Repositories;


use App\Models\Category;
use Illuminate\Support\Collection;
use Pros\CodeBase\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository
{
    protected $category;
    public function __construct(Category $category)
    {
        parent::__construct();
        $this->category = $category;
    }

    public function getAllCategory()
    {
        $paginate = 10;
       return $this->orderBy('id', 'DESC')->paginate($paginate);
    }

    public function getDetailCategory($id)
    {
        $this->getByIdCategory($id);
        return $this->get()->where('id', $id)->first();
    }

    public function getByIdCategory($id)
    {
        return $this->findOrFail($id);
    }

    public function getStoreCategory($request)
    {
        return $this->create($request);
    }

    public function getUpdateCategory($id , $request)
    {
        return $this->where('id', $id)->update(['name' => $request]);
    }

    public function getDestroyCategory($id)
    {
        $category = $this->getByIdCategory($id);
        return $category->delete();
    }

}
