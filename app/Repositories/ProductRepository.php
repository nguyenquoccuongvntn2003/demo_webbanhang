<?php

namespace App\Repositories;

use Illuminate\Support\Collection;
use Pros\CodeBase\Repositories\BaseRepository;
use App\Models\Product;
use App\Models\DB;
class ProductRepository extends BaseRepository
{
    public $product;
    public function __construct(Product $product)
    {
        parent::__construct();
        $this->product = $product;
    }

    public function all($id)
    {
        $paginate = 7;
        $query = $this->orderBy('id', 'DESC');
        if ($id) {
            $query = $query->where('category_id', $id);
        }
        return $query->paginate($paginate);
    }

    public function getDetailProduct($id)
    {
        $this->getByIdProduct($id);
        return $this->get()->where('id', $id)->first();
    }

    public function getByIdProduct($id)
    {
        return $this->findOrFail($id);
    }

    public function getStoreProduct($request)
    {
        // DB::beginTransaction();
        // try {
        //     DB::table('users')->update(['votes' => 1]);
        //     DB::table('posts')->delete();

        //     DB::commit();
        // } catch (Exception $e) {
        //     DB::rollBack();

        //     throw new Exception($e->getMessage());
        // }
            return $this->create($request);
    }

    public function getUpdateProduct($id, $request)
    {
        $product = $this->getByIdProduct($id);
        return $product->update($request);
    }

    public function getDestroyProduct($id)
    {
        $product = $this->getByIdProduct($id);
        return $product->delete();
    }

    public function getSearchProduct($data)
    {
        return $this->where('name', 'like', "%{$data}%")->get();
    }
}
