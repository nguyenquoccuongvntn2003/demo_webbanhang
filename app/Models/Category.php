<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = "categories";
    protected $fillable = [
        'name',
        'id'
    ];

    public static $rules = [
        'name' => 'required|string|max:50',
    ];
    public function product()
    {
        return $this->hasMany(Product::class);
    }

}
