<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "product";
    protected $fillable = [
        'name',
        'image',
        'quantity',
        'price',
        'status',
        'category_id',

    ];
    // public static $rules = [
    //     'name'              => 'required|string|max:50',
    //     'quantity'          => 'required|integer',
    //     'price'             => 'required|integer',
    //     'status'            => 'required|string',
    //     'image'             => 'require |string',
    //     'category_id'       => 'required',
    // ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
