<?php 

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{   
    protected $categoryRepository;
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getAll()
    {
        return $this->categoryRepository->getAllCategory();
    }

    public function getDetail($id)
    {
        return $this->categoryRepository->getDetailCategory($id);
    }

    public function getById($id)
    {
        return $this->categoryRepository->getByIdCategory($id);
    } 
    
    public function getStore($request)
    {
        return  $this->categoryRepository->getStoreCategory($request); 
    }

    public function update($id,$request)
    {
        return $this->categoryRepository->getUpdateCategory($id, $request);
    }

     public function getDestroy($id)
    {
        return $this->categoryRepository->getDestroyCategory($id);
    }
}
