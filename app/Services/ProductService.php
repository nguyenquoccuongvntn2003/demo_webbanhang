<?php

namespace App\Services;

use App\Repositories\ProductRepository;

class ProductService

{
    protected $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        $this ->productRepository = $productRepository;
    }

    public function getIndex($id)
    {
        return $this->productRepository->all($id);
    }

    public function getDetail($id)
    {
        return $this->productRepository->getDetailProduct($id);
    }

    public function getById($id)
    {
        return $this->productRepository->getByIdProduct($id);
    }

    public function getStore($request)
    {
        return  $this->productRepository->getStoreProduct($request);
    }

    public function update($id,$request)
    {
        return $this->productRepository->getUpdateProduct($id, $request);
    }

     public function getDestroy($id)
    {
        return $this->productRepository->getDestroyProduct($id);
    }

    public function search($data)
    {
        return $this->productRepository->getSearchProduct($data);
    }

}
